#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void* pi(void* tid);
double sumaTotal = 0.0;
long nElementos;
int nHilos;

int main(int argc,char* argv[])
{
    int estado;
    long h;
    pthread_t *hilos;

    nHilos = strtol(argv[1], NULL, 10);
    nElementos = strtol(argv[2], NULL, 10);
    //printf("%ld %ld\n",nHilos,nElementos);
    hilos = malloc(nHilos*sizeof(pthread_t));//hilos = new pthread_t[nHilos];


    for(h = 0;h < nHilos; h++)
    {
        estado = pthread_create(&hilos[h],NULL, pi,(void*) h);
        if(estado != 0)
        {
            printf("Error al crear el hilo %ld\n",h);
            exit(-1);
        }
    }
    for(h = 0;h < nHilos; h++)
    {
      estado = pthread_join(hilos[h],NULL);
      if(estado != 0)
      {
          printf("Error al hacer join con el hilo %ld\n",h);
          exit(-1);
      }
    }

    printf("El valor estimado  de pi es: %.15f",4*sumaTotal);

    free(hilos);

    return 0;
}

void* pi(void* tid)
{
    long id = (long)tid;
    long n = nElementos/nHilos;
    long ini =id*n;
    long fin = ini + n -1;
    double signo = 1.0;

    printf("%ld %ld %ld\n",n,ini, fin);

    if(ini%2 != 0)
    {
        signo = -1.0;
    }

    for(long i = ini; i<=fin;i++,signo *= -1.0)
    {
        sumaTotal += signo/(2*i+1);
    }

    pthread_exit(NULL);
}
